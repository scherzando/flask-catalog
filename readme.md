# Flask-catalog

A simple CRUD app to display a catalog of items.
Users can log in with a Google account, view any item, and update/delete their own items.
This project was built as part of Udacity's Full-Stack Nanodegree course.

## Requirements
* Python 2.7
* Flask
* SQLAlchemy

## Dev environment setup
This app is developed in a virtual Linux machine. If you have Vagrant and VirtualBox installed, you can create an identical VM using the included Vagrantfile from Udacity. Once this is done, the app will be located at vagrant/flask-catalog.

Note that the client_secret.json file is not included. This is required for the app to authenticate users with Google. To obtain the file:
* Register a new OAuth client ID project at console.developers.google.com/apis
* In the project dashboard, enter the following restrictions:
    * Authorised JavaScript origins: http://localhost:5000, http://localhost:5001
    * Authorised redirect URIs: http://localhost:5000/catalog, http://localhost:5001/catalog
* Download the json file from Google
* Place it in the same directory as application.py
* Rename it to client_secret.json

## To run the app
* python application.py
The app will be available at localhost:5000, or localhost:5001 if you are using the VM.
