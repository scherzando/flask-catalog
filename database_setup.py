'''
database_setup
Creates an empty database for the Catalog app.
'''
from sqlalchemy import Column, ForeignKey, Integer, String, Index
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine


'''
Constants
'''
database_name = 'sqlite:///categories.db'
Base = declarative_base()


'''
Table definitions
'''


class Image(Base):
    __tablename__ = 'image'
    id = Column(Integer, primary_key=True)
    filepath = Column(String(1000), nullable=False)


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    email = Column(String(250), nullable=False)
    picture = Column(String(1000))


class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    title = Column(String(250), nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship(User)
    image_id = Column(Integer, ForeignKey('image.id'))
    image = relationship(Image)

    @property
    def serialize(self):
        image = ''
        if self.image is not None:
            image = self.image.filepath
        return {
            'id': self.id,
            'title': self.title,
            'image': image
        }


class Item(Base):
    __tablename__ = 'item'
    id = Column(Integer, primary_key=True)
    title = Column(String(250), nullable=False)
    description = Column(String(1000))
    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship(Category)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship(User)
    image_id = Column(Integer, ForeignKey('image.id'))
    image = relationship(Image)

    @property
    def serialize(self):
        image = ''
        if self.image is not None:
            image = self.image.filepath
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'category': self.category.title,
            'image': image}


# Create indexes on common lookup columns
Index('category-title', Category.title)
Index('item-title', Item.category_id, Item.title)


# Create the database
engine = create_engine(database_name)
Base.metadata.create_all(engine)
