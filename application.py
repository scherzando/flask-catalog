'''
Imports
'''
from flask import (
    Flask,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
    send_from_directory
)
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from database_setup import Base, User, Category, Item, Image
import httplib2
import json
import os

# the following imports are for authentication via Google
from flask import make_response, session as login_session
import random
import requests
import string
from oauth2client.client import flow_from_clientsecrets, FlowExchangeError


'''
Constants
'''
DATABASE_NAME = 'sqlite:///categories.db'
CLIENT_ID = json.loads(
    open('client_secret.json', 'r').read())['web']['client_id']
# The upload folder is for storing images
UPLOAD_FOLDER = os.path.basename('uploads')


'''
Initial setup
'''
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024  # maximum payload of 2MB
engine = create_engine(DATABASE_NAME)
Base.metadata.bind = create_engine
DBSession = scoped_session(sessionmaker(bind=engine))


'''
Route definitions
'''


@app.route('/')
@app.route('/catalog/')
def viewCatalog():
    # display all the categories in the catalog
    session = DBSession()
    categories = session.query(Category).outerjoin(Image).all()

    template = render_template(
        'catalog.html',
        categories=categories)
    session.close()
    return template


@app.route('/catalog/new/', methods=['GET', 'POST'])
def newCategory():
    # verify user is logged in
    if 'username' not in login_session:
        return redirect('/login')

    if request.method == 'POST':
        # we won't accept an empty title
        form_title = request.form['title'].strip()
        if len(form_title) == 0:
            # redirect the user with instructions
            flash("Please enter a valid title")
            return redirect(url_for('newCategory'))

        # create a new category and view it
        new_category = Category(title=form_title,
                                user_id=login_session['user_id'])
        if 'image' in request.files:
            image = newImage(request.files['image'])
            new_category.image = image

        session = DBSession()
        session.add(new_category)
        session.commit()
        session.close()
        flash('Category successfully added')
        return redirect(url_for('viewCatalog'))
    else:
        # display the form for creating a new category
        return render_template('new-category.html')


@app.route('/catalog/<string:category_title>/')
def viewCategory(category_title):
    # retrieve the category and all its items for display
    session = DBSession()
    category = session.query(Category).outerjoin(Image).filter(
        Category.title == category_title).one()

    items = session.query(Item).outerjoin(Image).filter(
        Item.category_id == category.id).all()

    template = render_template(
        'view-category.html',
        category=category,
        items=items)
    session.close()
    return template


@app.route('/catalog/<string:category_title>/update/', methods=['GET', 'POST'])
def updateCategory(category_title):
    # verify user is logged in
    if 'username' not in login_session:
        return redirect('/login')

    # retrieve the category in its current state
    session = DBSession()
    category = session.query(Category).outerjoin(Image).filter(
        Category.title == category_title
        ).one()

    if request.method == 'POST':
        # verify the user owns this category
        if login_session['user_id'] != category.user_id:
            flash("You don't have permission to update %s" % category_title)
            return redirect(url_for('viewCatalog'))

        # verify the form isn't totally empty
        form_title = request.form['title'].strip()
        if len(form_title) == 0 and 'image' not in request.files:
            flash("Please enter your updates and resubmit")
            return redirect(url_for('updateCategory',
                            category_title=category_title))

        # update the category
        if len(form_title) > 0 and category.title != request.form['title']:
            category.title = form_title
        if 'image' in request.files:
            image = newImage(request.files['image'])
            session.add(image)
            category.image = image
            # todo: allow existing images to be deleted

        session.add(category)
        session.commit()
        category_title = category.title
        session.close()
        flash("Category successfully updated")

        # redirect to view the newly added category
        return redirect(url_for('viewCategory', category_title=category_title))
    else:
        # display the category update form
        template = render_template('update-category.html', category=category)
        session.close()
        return template


@app.route('/catalog/<string:category_title>/delete/', methods=['GET', 'POST'])
def deleteCategory(category_title):
    # verify user is logged in
    if 'username' not in login_session:
        return redirect('/login')

    # retrieve the category in its current state
    session = DBSession()
    category = session.query(Category).outerjoin(Image).filter(
        Category.title == category_title).one()

    if request.method == 'POST':
        # verify the user owns this category
        if login_session['user_id'] != category.user_id:
            flash("You don't have permission to update %s" % category_title)
            return redirect(url_for('viewCatalog'))

        # delete the category and its items
        items = session.query(Item).filter(
            Item.category_id == category.id).all()
        for item in items:
            session.delete(item)
        session.delete(category)
        session.commit()
        session.close()

        flash("Category successfully deleted")
        return redirect(url_for('viewCatalog'))
    else:
        # display the category delete form
        template = render_template('delete-category.html', category=category)
        session.close()
        return template


@app.route('/catalog/<string:category_title>/new', methods=['GET', 'POST'])
def newItem(category_title):
    # verify user is logged in
    if 'username' not in login_session:
        return redirect('/login')

    # retrieve the category
    session = DBSession()
    category = session.query(Category).filter_by(title=category_title).one()

    if request.method == 'POST':
        # verify the user owns this category
        if login_session['user_id'] != category.user_id:
            flash("You don't have permission to update %s" % category_title)
            return redirect(url_for('viewCatalog'))

        # validate user input
        form_description = request.form['description'].strip()
        form_title = request.form['title'].strip()
        # we will not accept an empty title
        if len(form_title) == 0:
            # redirect the user with instructions
            flash('Please enter a valid title')
            return redirect(url_for('newItem', category_title=category_title))

        # create a new item in this category
        new_item = Item(
            title=form_title,
            description=form_description,
            category_id=category.id,
            user_id=category.user_id
            )
        if 'image' in request.files:
            image = newImage(request.files['image'])
            session.add(image)
            new_item.image = image

        session.add(new_item)
        session.commit()
        session.close()
        flash("New item successfully added")

        # redirect to the category
        return redirect(url_for('viewCategory', category_title=category_title))
    else:
        # display the new item form
        template = render_template('new-item.html', category=category)
        session.close()
        return template


@app.route('/catalog/<string:category_title>/<string:item_title>/')
def viewItem(category_title, item_title):
    # retrieve the item and its category for display
    session = DBSession()
    category = session.query(Category).outerjoin(Image).filter(
        Category.title == category_title).one()

    item = session.query(Item).outerjoin(Image).filter(
        Item.category_id == category.id).filter(Item.title == item_title).one()

    template = render_template(
        'view-item.html',
        category=category,
        item=item
        )
    session.close()
    return template


@app.route(
    '/catalog/<string:category_title>/<string:item_title>/update/',
    methods=['GET', 'POST'])
def updateItem(category_title, item_title):
    # verify user is logged in
    if 'username' not in login_session:
        return redirect('/login')

    # retrieve the item and its category
    session = DBSession()
    category = session.query(Category).filter_by(title=category_title).one()

    item = session.query(Item).filter_by(
        category_id=category.id, title=item_title).one()

    if request.method == 'POST':
        # verify the user owns the item
        if login_session['user_id'] != item.user_id:
            flash("You don't have permission to update %s" % category_title)
            return redirect(url_for('viewCatalog'))

        # exit if the form is completely empty
        form_title = request.form['title'].strip()
        form_description = request.form['description'].strip()
        if (len(form_title) == 0 and
                len(form_description) == 0 and
                'image' not in request.files):
            flash("Please enter your updates and resubmit")
            return redirect(url_for(
                'updateItem',
                category_title=category_title,
                item_title=item_title))

        # we don't want to accept an empty title
        # if the title is empty we will just keep the existing one
        if len(form_title) > 0 and item.title != form_title:
            item.title = form_title

        # description can be empty
        if item.description != form_description:
            item.description = form_description

        # image is optional
        if 'image' in request.files:
            image = newImage(request.files['image'])
            session.add(image)
            item.image = image
            # todo: allow existing images to be deleted

        session.add(item)
        session.commit()
        item_title = item.title
        session.close()
        flash("Item successfully updated")

        # redirect to view the updated item
        return redirect(url_for(
            'viewItem',
            category_title=category_title,
            item_title=item_title)
            )
    else:
        # display the update item form
        template = render_template(
            'update-item.html',
            category=category,
            item=item)
        session.close()
        return template


@app.route(
    '/catalog/<string:category_title>/<string:item_title>/delete/',
    methods=['GET', 'POST'])
def deleteItem(category_title, item_title):
    # verify user is logged in
    if 'username' not in login_session:
        return redirect('/login')

    # retrieve the item and its category
    session = DBSession()
    category = session.query(Category).filter_by(title=category_title).one()

    item = session.query(Item).filter_by(
        category_id=category.id, title=item_title).one()

    if request.method == 'POST':
        # verify the user owns the item
        if login_session['user_id'] != item.user_id:
            flash("You don't have permission to update %s" % category_title)
            return redirect(url_for('viewCatalog'))

        # delete the item from the database
        session.delete(item)
        session.commit()
        session.close()
        flash("Item successfully deleted")

        # redirect to view the category
        return redirect(url_for('viewCategory', category_title=category_title))
    else:
        # display the delete item form
        template = render_template(
            'delete-item.html',
            category=category,
            item=item
            )
        session.close()
        return template


@app.route('/login/')
def showLogin():
    # state is a random string to identify the login session
    state = ''.join(random.choice(
        string.ascii_uppercase + string.digits) for x in xrange(32))
    login_session['state'] = state
    return render_template('login.html', STATE=state)


@app.route('/uploads/<path:filepath>/')
def serveUpload(filepath):
    # returns files from the upload folder
    # this is currently only used for images
    return send_from_directory(UPLOAD_FOLDER, filepath)


'''
API endpoints
'''


@app.route('/catalog/JSON/')
def catalogJSON():
    # return a list of all categories in the catalog
    session = DBSession()
    catalog = session.query(Category).all()
    json = jsonify(Categories=[c.serialize for c in catalog])
    session.close()
    return json


@app.route('/catalog/<string:category_title>/JSON/')
def categoryJSON(category_title):
    # return a single category
    session = DBSession()
    category = session.query(Category).filter_by(title=category_title).one()
    json = jsonify(Category=category.serialize)
    session.close()
    return json


@app.route('/catalog/<string:category_title>/<string:item_title>/JSON')
def itemJSON(category_title, item_title):
    # return a single item
    session = DBSession()
    category = session.query(Category).filter_by(title=category_title).one()
    item = session.query(Item).filter_by(
        category_id=category.id, title=item_title).one()
    json = jsonify(Item=item.serialize)
    session.close()
    return json


'''
The following methods are for authentication via Google
'''


@app.route('/gconnect', methods=['POST'])
def gconnect():
    # validate state token
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # obtain authorisation code
    code = request.data

    # update the one-time-code into a credentials object
    try:
        oauth_flow = flow_from_clientsecrets('client_secret.json', scope='')
        oauth_flow.redirect_uri = 'postmessage'
        credentials = oauth_flow.step2_exchange(code)
    except FlowExchangeError:
        response = make_response(json.dumps(
            'Failed to update the auth code'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # validate the access code with Google
    access_token = credentials.access_token
    url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?'
    url += 'access_token=%s' % access_token
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])

    # exit if there was an error with the access token
    if result.get('error') is not None:
        response = make_response(json.dumps(result.get('error')), 500)
        response.headers['Content-Type'] = 'application/json'
        return response

    # verify the token vs. the user
    gplus_id = credentials.id_token['sub']
    if result['user_id'] != gplus_id:
        response = make_response(json.dumps(
            'Token user ID does not match expected user ID'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # verify the token vs. this app
    if result['issued_to'] != CLIENT_ID:
        response = make_response(json.dumps(
            'Token client ID does not match the app'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # check if user is already connected
    stored_access_token = login_session.get('access_token')
    stored_gplus_id = login_session.get('gplus_id')
    if stored_access_token is not None and gplus_id == stored_gplus_id:
        response = make_response(json.dumps(
            'Current user already connected'), 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    # store the access token in the session for later reference
    login_session['access_token'] = credentials.access_token
    login_session['gplus_id'] = gplus_id

    # get some more user info from Google
    userinfo_url = 'https://www.googleapis.com/oauth2/v1/userinfo'
    params = {'access_token': credentials.access_token, 'alt': 'json'}
    answer = requests.get(userinfo_url, params=params)

    # store the username, email and picture in the login session
    data = answer.json()
    login_session['username'] = data['name']
    login_session['email'] = data['email']
    login_session['picture'] = data['picture']

    # see if user already exists and create one if not
    user_id = getUserID(login_session['email'])
    if not user_id:
        user_id = createUser(login_session)

    # store user ID
    login_session['user_id'] = user_id
    output = '<h1>Welcome, '
    output += login_session['username']
    output += '!</h1>'
    output += '<img src="'
    output += login_session['picture']
    output += ' " style = "width: 300px; height: 300px;border-radius: 150px;'
    output += '-webkit-border-radius: 150px;-moz-border-radius: 150px;"> '
    flash("You are now logged in as %s" % login_session['username'])
    return output


@app.route('/gdisconnect')
def gdisconnect():
    access_token = login_session.get('access_token')

    # early exit if disconnecting is unnecessary
    if access_token is None:
        response = make_response(json.dumps('Current user not connected'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # revoke the token with google
    url = 'https://accounts.google.com/o/oauth2/revoke?'
    url += 'token=%s' % login_session['access_token']
    h = httplib2.Http()
    result = h.request(url, 'GET')[0]
    if result['status'] == '200':
        del login_session['access_token']
        del login_session['gplus_id']
        del login_session['username']
        del login_session['email']
        del login_session['picture']
        del login_session['user_id']
        response = make_response(json.dumps('Successfully disconnected'), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
    else:
        response = make_response(json.dumps(
            'Failed to revoke token for the current user'), 400)
        response.headers['Content-Type'] = 'application/json'
        return response


'''
Helper methods to identify and create users
'''


def getUserID(email):
    # return user id if we have an id stored for this email address
    session = DBSession()
    try:
        user = session.query(User).filter_by(email=email).one()
        user_id = user.id
        session.close()
        return user_id
    except:
        session.close()
        return None


def getUserInfo(user_id):
    # takes a user id and returns their user info
    # this method assumes the user exists
    session = DBSession()
    user = session.query(User).filter_by(id=user_id).one()
    session.close()
    return user


def createUser(login_session):
    # creates a user and returns the new user id
    # this method assumes the user doesn't exist
    new_user = User(
        name=login_session['username'],
        email=login_session['email'],
        picture=login_session['picture']
        )
    session = DBSession()
    session.add(new_user)
    session.commit()
    user = session.query(User).filter_by(email=login_session['email']).one()
    session.close()
    return user.id


'''
Helper method for uploading images.
'''


def newImage(file):
    # This takes an image file and saves it in the uploads folder.
    # Returns a new Image object.
    filepath = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
    file.save(filepath)
    new_image = Image(filepath=file.filename)
    return new_image


'''
Run the Flask app if this is called as the main class.
'''
if __name__ == '__main__':
    app.secret_key = 'super-secret-key'  # todo: update this
    app.debug = True  # todo: disable debug mode
    app.run(host='0.0.0.0', port=5000)
